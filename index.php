<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="HandheldFriendly" content="True">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Project Alpha</title>
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/media.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
<meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,height=device-height,target-densitydpi=device-dpi,user-scalable=yes" />
	

</head>
<body>
<div class="container-full">
	<div class="row">
		<div class="col-lg-12">
			
			<!-- start -->
			<!-- section-1 -->
			<!-- start -->
			<div id="section-1">

				<!-- start -->
				<!-- top-section logo and home, contactus and login -->
				<!-- start -->
				<div class="row">
					<div class="col-lg-6 col-xs-12 logo-section">
						<a href="#">
							<img src="img/logo.jpg" alt="">
						</a>
					</div>
					<div class="col-lg-6 col-xs-12">
						<ul class="inline-ul home-login">
							<li>
								<a href="#" class="active">
									Home
								</a>
							</li>
							<li>
								<a href="#">
									Contact Us
								</a>
							</li>
							
						</ul>
					</div>
				</div>
				<!-- end -->
				<!-- top-section logo and home, contactus and login -->
				<!-- end -->
				
				<!-- start -->
				<!-- DIABETES 360 Section -->
				<!-- start -->
				<div class="row">
					<div class="col-lg-5 col-xs-6 col-lg-offset-2">
						<div class="section-1-content">
							<h1>DIABETES 360 - <span>degrees</span></h1>
							<p>
								Cura is a mobile platform to manage your diabetes and all activities around it.
							</p>
						</div>
						<div class="scroll-down-1 scroll-down">
							<img src="img/ic_scroll-down.png" alt="">
						</div>
					</div>
					<div class="col-lg-5 col-xs-6">
						<div class="mobile-img">
							<img src="img/phone_with_screen.png" alt="Mobile Phone">
						</div>
						
					</div>
				</div>
				<!-- end -->
				<!-- DIABETES 360 Section -->
				<!-- end -->
				
				<!-- start -->
				<!-- scroll and apps-icon section -->
				<!-- start -->
				<div class="row">
					<!-- <div class="col-lg-5 col-lg-offset-2">
						<div class="scroll-down-1 scroll-down">
							<img src="img/ic_scroll-down.png" alt="">
						</div>
					</div> -->
					<!-- <div class="col-lg-5">
						<div class="app-icon">
							<ul class="inline-ul">
								<li>
									<a href="#">
										<img src="img/ic_appstore.png" alt="">
									</a>
								</li>
								<li>
									<a href="#">
										<img src="img/ic_playstore.png" alt="">
									</a>
								</li>
							</ul>
						</div>
					</div> -->
				</div>
				<!-- end -->
				<!-- scroll and apps-icon section -->
				<!-- end -->

			</div>
			<!-- end -->
			<!-- section-1 -->
			<!-- end -->


			<!-- start -->
			<!-- menu section... -->
			<!-- start -->
			<div class="menu-section">
				<nav>
					<div class="row">
						<div class="col-5">
							<a href="#section-2">
								<img src="img/ic_medi_manage_small.png" alt="">
								<span>Medication Management</span>
							</a>
						</div>
						<div class="col-5">
							<a href="#section-3">
								<img src="img/ic_vitals_small.png" alt="">
								<span>Vitals</span>
							</a>
						</div>
						<div class="col-5">
							<a href="#section-4">
								<img src="img/ic_lifestyle.png" alt="">
								<span>Lifestyle</span>
							</a>
						</div>
						<div class="col-5">
							<a href="#section-5">
								<img src="img/ic_symtoms.png" alt="">
								<span>Symtoms</span>
							</a>
						</div>
						<div class="col-5">
							<a href="#section-6">
								<img src="img/ic_report.png" alt="">
								<span>Report</span>
							</a>
						</div>
					</div>
				</nav>
			</div>
			<!-- end -->
			<!-- menu section... -->
			<!-- end -->
			
			<!-- start -->
			<!-- section-2 -->
			<!-- start -->
			<div id="section-2" class="section-left">
				<div class="row">
					<div class="col-lg-9 col-xs-12 float-right">
						<div class="tablet-icon">
							<img src="img/ic_medi_manament_big.png" alt="">
						</div>
						<h1>
							Medication <span>Management</span>
						</h1>
						<p>
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
						</p>
						<!-- <div class="scroll-down-1 scroll-down">
							<img src="img/ic_scroll-down.png" alt="">
						</div> -->
					</div>
					<div class="col-lg-3 col-xs-12 mobile-img float-left">
						<img src="img/phone_with_screen1.png" alt="">
					</div>
				</div>
			</div>
			<!-- end -->
			<!-- section-2 -->
			<!-- end -->

			<!-- start -->
			<!-- section-3 -->
			<!-- start -->
			<div id="section-3" class="section-right">
				<div class="row">
					<div class="col-lg-8 col-xs-12 col-lg-offset-1 content-section">
						<div class="tablet-icon">
							<img src="img/ic_vitals_big.png" alt="">
						</div>
						<h1>
							Vitals
						</h1>
						<p>
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
						</p>
						<!-- <div class="scroll-down-1 scroll-down">
							<img src="img/ic_scroll-down.png" alt="">
						</div> -->
					</div>
					<div class="col-lg-3 col-xs-12 mobile-img">
						<img src="img/phone_with_screen2.png" alt="">
					</div>
				</div>
			</div>
			<!-- end -->
			<!-- section-3 -->
			<!-- end -->
			
			<!-- start -->
			<!-- section-4 -->
			<!-- start -->
			<div id="section-4" class="section-left">
				<div class="row">
					<div class="col-lg-9 col-xs-12 float-right">
						<div class="tablet-icon">
							<img src="img/image_2.png" alt="">
						</div>
						<h1>
							lifestyle <span>Management</span>
						</h1>
						<p>
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
						</p>
						<!-- <div class="scroll-down-1 scroll-down">
							<img src="img/ic_scroll-down.png" alt="">
						</div> -->
					</div>
					<div class="col-lg-3 col-xs-12 mobile-img float-left">
						<img src="img/phone_with_screen3.png" alt="">
					</div>
				</div>
			</div>
			<!-- end -->
			<!-- section-4 -->
			<!-- end -->
			
			<!-- start -->
			<!-- section-5 -->
			<!-- start -->
			<div id="section-5" class="section-right">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-1 col-xs-12 content-section">
						<div class="tablet-icon">
							<img src="img/image_3.png" alt="">
						</div>
						<h1>
							Symptoms
						</h1>
						<p>
							Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
						</p>
						<!-- <div class="scroll-down-1 scroll-down">
							<img src="img/ic_scroll-down.png" alt="">
						</div> -->
					</div>
					<div class="col-lg-3 col-xs-12 mobile-img">
						<img src="img/phone_with_screen4.png" alt="">
					</div>
				</div>
			</div>
			<!-- end -->
			<!-- section-5 -->
			<!-- end -->

			<!-- start -->
			<!-- section-6 -->
			<!-- start -->
			<div id="section-6">
				<div class="row">
					<div class="col-lg-4 col-xs-12 content-section">
						<div class="tablet-icon">
							<img src="img/image_5.png" alt="">
						</div>
						<h1>
							Share <span>Report</span>
						</h1>
						<p>
							Get all your diabetes medicines and supplies at your doorstep.
							Subscribe to Auto-Refill services to automatically get your
							monthly refill pack or order medicines as and when you
							need them. Adhere to your medication schedule by 
							setting up reminders to take medicines.
						</p>
						<!-- <div class="scroll-down-1 scroll-down">
							<img src="img/ic_scroll-down.png" alt="">
						</div> -->
					</div>
					<div class="col-lg-8 col-xs-12 report-img">
						<img src="img/image_4.png" alt="">
					</div>
				</div>
			</div>
			<!-- end -->
			<!-- section-6 -->
			<!-- end -->

			<!-- start -->
			<!-- footer section -->
			<!-- start -->
			<div id="header-section">
				<div class="row">
					<div class="col-lg-6 col-centered">
						<h2>GET IN TOUCH WITH US</h2>
						<ul class="inline-ul">
							<li>
								<a href="#">
									<img src="img/facebook.png" alt="" class="facebook">
								</a>
							</li>
							<li>
								<a href="#">
									<img src="img/twitter.png" alt="" class="twitter">
								</a>
							</li>
							<li>
								<a href="#">
									<img src="img/youtube.png" alt=""  class="youtube">
								</a>
							</li>
							<li>
								<a href="#">
									<img src="img/linkedin.png" alt="" class="linkedin">
								</a>
							</li>
						</ul>
						<div class="row form">
							<div class="col-lg-6 col-xs-10 form-field">
								<div class="textbox">
									<input type="text" class="form-control" placeholder="Name">
								</div>
								<div class="textbox" style="margin-top:20px;">
									<input type="text" class="form-control" placeholder="Email Id">
								</div>
								<div class="textbox" style="margin-top:20px;">
									<input type="text" class="form-control" placeholder="Mobile No.">
								</div>
							</div>
							<div class="col-lg-6 col-xs-10 form-field">
								<div class="textbox">
									<textarea name="" class="form-control" id="" placeholder="Message.."></textarea>
								</div>
								<div class="send">
									<button>Send</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end -->
			<!-- footer section -->
			<!-- end -->
			
			<div class="last-strip">
				<ul class="inline-ul">
					<li>
						<a href="#">Tems and Conditions</a>
					</li>
					<li> | </li>
					<li>
						<a href="#">Privacy Policy</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	
</div>
	<div class="test hidden"></div>
</body>
</html>
<script type="text/javascript">
jQuery(document).ready(function($) {
	// $('body').mousedown(function(e){if(e.button==1)return false});
	
	$(window).scroll(function() {
		var width = $(window).width();
		// offset =  $(".main-menu").offset();
		scrollTop =  $(window).scrollTop();
		x = $( window ).height() - 50;
		if($(".test").text() == "")
		{
			$(".test").text(x);
		}
		var y = $(".test").text();
		if (scrollTop > y) {
			$("nav").addClass('fixed');
		} else {
			$("nav").removeClass('fixed');
		}
	});


	var win_height = $( window ).height() - 50;
	$("#section-1").height(win_height);

});


	$(function() {
	  $('a[href*=#]:not([href=#])').click(function() {
	  	$("a").removeClass('active');
	  	$(this).addClass('active');
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	    	var menuheight = $(".menu-section").outerHeight()
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top - menuheight
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});

	$(".scroll-down-1").click(function() {
		$("html,body").animate({
	      scrollTop: $("#section-2").offset().top - 49
	    }, 1000);
	});

	var menuheight = $(".menu-section").outerHeight();
	menuItems = $(".menu-section").find("a"),
	scrollItems = menuItems.map(function(){
	      var item = $($(this).attr("href"));
	      if (item.length) { return item; }
	});
	console.log(scrollItems);
	$(window).scroll(function(){

		var fromTop = $(this).scrollTop();
		// console.log(fromTop);
		   // Get id of current scroll item
		var cur = scrollItems.map(function(){
		 if ($(this).offset().top - 100 < fromTop)
		   return this;
		});

		// Get the id of the current element
	   cur = cur[cur.length-1];
	   var id = cur && cur.length ? cur[0].id : "";

		// console.log(id);
		$(".menu-section").find('a').removeClass('active');
		($('a[href="#'+id+'"]').addClass('active'));

	});

</script>